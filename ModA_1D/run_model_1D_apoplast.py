from model_1D_apoplast import Mod1D
import numpy as np
import pylab as plt
import random

n=100
k=2

auxin=6
pin=4

time=10000

# base auxin and pin with random noise
noise=.01
#random.seed(2356)
aux = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
apo = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
pin_left = [pin for i in range(n)] + np.random.normal(0,pin*noise,n)
pin_right = [2*pin-x for x in pin_left] + np.random.normal(0,pin*noise,n)

#set noise manually
#aux[9]*=1.1
#aux[29]*=1.1
#aux[49]*=1.1
#aux[69]*=1.1

start=np.concatenate((aux, apo, pin_left, pin_right))

PBC=1

# class initiation
model=Mod1D(n,k,PBC)

model.Ga=1 # auxin degredation/synthesis coefficient to A
model.A=6 # auxin base level of synthesis (steady state)
model.Da=35.5 # auxin diffusion coefficient to equilibrium
model.Da_sym=0 # symplast diffusion coefficient (cell-cell)
model.Da_apo=0 # apoplast diffusion coefficient (apoplast-apoplast)

model.Gp=2 # pin degredation/synthesis coefficient to p
model.p=40 # pin base level of synthesis (steady state)
model.m=4 # influence of regulatory function phi
model.Ep=1 # pin efflux efficiency

model.q=10 # concentration of influx carriert on membrane side
model.Eq=1 # efficiency of influx carrier
model.V=1 # volume ratio of apoplast to cytoplast


# integration
T,R=model.intgr(time,start)
#print(model.sim(time,start))

# plots
#    concentration of auxin and PIN over time
def CT(R=R):
    [plt.plot(range(time),R[1:,:n])]
    plt.title("Auxin")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.xlim((0,1000))
    #plt.ylim((1.6,2.2))
    plt.show()
    
    plt.plot(range(time),R[1:,n:2*n])
    plt.title("PIN_left")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()
    
    plt.plot(range(time),R[1:,2*n:])
    plt.title("PIN_right")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()

#   concentration of auxin in cytosol and apoplast over all cells (lines: show curve plot; bars: show barplot)
def apo(lines=0,bars=1,R=R):
    fig=plt.figure(figsize=(15,5))
    ax=plt.subplot(111)
    ind=np.arange(1,n+1)
    bw=0.35
    plt.plot(range(1,n+1),R[0,:n],color="r",label="initial auxin levels (cytoplast)")
    #plt.plot(range(1,n+1),R[0,n:2*n],color="tomato",label="initial auxin levels (apoplast)")
    if bars:
        ax.bar(ind,R[-1,n:2*n],bw,label="apoplast auxin",color="darkorange")
        ax.bar(ind+bw,R[-1,:n],bw,label="cytoplast auxin",color="cornflowerblue")
    if lines:
        ax.plot(ind,R[-1,n:2*n],"darkorange")
        ax.plot(ind+bw,R[-1,:n],"cornflowerblue")
    ax.set_xticks([int(ind[i-1]+bw/2) for i in range(1,n+1) if i%5==0])
    plt.xlabel("cells")
    plt.ylabel("auxin concentration")
    plt.legend()
    fig.tight_layout()
    plt.show()
    
#   barplot of auxin levels over all cells (base: show initial auxin distribution)
def pattern(R=R,base=1):
    print()
    print("max:","{0:.3f}".format(max(R[-1,:n])))
    print("min:","{0:.3f}".format(min(R[-1,:n])))
    
    fig=plt.figure(figsize=(15,5))
    plt.bar(range(n),R[-1,:n],color="lightblue",label="auxin levels at steady state")
    if base: plt.plot(range(n),start[:n],"r",label="initial auxin levels")
    plt.ylabel("auxin concentration")
    plt.xlabel("cell position")
    if n>50: plt.xticks(range(0,n,5))
    else: plt.xticks(range(1,n+1))
    plt.title("Auxin pattern (noise: {}%)".format(noise*100))
    plt.legend(loc="lower left")
    plt.show()

def par2par(n,k,s):
    rang=(k-n)/s
    print(rang)
    plt.figure(figsize=(20,20))
    #fig, ax = plt.subplots(rang, 3, sharex='col', sharey='row')
    for i in range(n,k,s):
        for j in range(n,k,s):
            print(i*rang+j)
            model.Da=i
            model.sym_diff
            T,R=model.intgr(time,start)
            plt.subplot(rang,rang,(i-n)/10*rang+(j-n)/10+1)
            plt.plot(range(n),R[:n],"blue")
            plt.plot(range(n),R[n:2*n],"orange")
            plt.show()
#par2par(50,110,10)
    
# select plots to be shown after integration
#CT()
#pattern()
apo(lines=0,bars=1)
