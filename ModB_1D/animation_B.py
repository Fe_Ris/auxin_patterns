#from run_model_1D_apoplast_B6 import R,n
#import numpy as np
#import pylab as plt
#from matplotlib import animation
#
#fig=plt.figure(figsize=(15,5))
#ax=plt.axes(ylim=(0,max(R[:,:2*n].flatten())))
##plt.ylim((0,max(R[:,:2*n].flatten())))
#
#barA = ax.bar(np.arange(n)-.2,R[0,:n],width=0.4,align="center",color="cornflowerblue")
#lineA, = ax.plot([],[],"cornflowerblue")
#
#barB = ax.bar(np.arange(n)+.2,R[0,:n],width=0.4,align="center",color="darkorange")
#lineB, = ax.plot([],[],"darkorange")
#
##Writer=animation.writers['ffmpeg']
##writer=Writer(fps=10,bitrate=1800)
#
#def animate(i):
#    [barA[j].set_height(R[i,j]) for j in range(n)]
#    [lineA.set_data(np.arange(n)-.2,R[i,:n])]
#    [barB[j].set_height(R[i,j+n]) for j in range(n)]
#    [lineB.set_data(np.arange(n)+.2,R[i,n:2*n])]
#    #plt.ylim((0,max(R[:,:2*n].flatten())))
#    plt.title("t="+str(i))
#    return barA,barB
#
#ani=animation.FuncAnimation(fig,animate,frames=1000,interval=200)
#plt.plot(range(n),R[0,:n],"r")
#plt.show()


from run_model_1D_apoplast_B6 import R,n
import numpy as np
import pylab as plt
from matplotlib import animation


fig=plt.figure(figsize=(15,5))
ax=plt.axes()
#ax2=ax.twinx()

ax.set_xlabel("cells")
ax.set_ylabel("Auxin concentration")
#ax2.set_ylabel("PIN concentration")

ax.set_ylim((0,5))
#ax2.set_ylim((-50,100))
#ax2.set_yticks(label=range(150))

barA = ax.bar(np.arange(n)-.2,R[0,:n],width=0.4,align="center",color="cornflowerblue",label="cytoplast")
barB = ax.bar(np.arange(n)+.2,R[0,:n],width=0.4,align="center",color="darkorange",label="apoplast")
#line_pl, = ax2.plot(range(n),R[0,:n],color="r",label="PIN left")
#line_pr, = ax2.plot(range(n),R[0,:n],color="g",label="PIN right")

#all=[barA,barB,line_pl,line_pr]
#labels=[x.get_label() for x in all]
ax.legend()

def animate(i):
    for j in range(n):
        [barA[j].set_height(R[i,j]) for j in range(n)]
        [barB[j].set_height(R[i,j+n]) for j in range(n)]
    #line_pl.set_data(range(n),R[i,2*n:3*n])
    #line_pr.set_data(range(n),R[i,3*n:])
    ax.set_title("t="+str(i))
    return barA,barB

Writer=animation.writers['ffmpeg']
writer=Writer(fps=5)

ani=animation.FuncAnimation(fig,animate,frames=500)
ani.save("1D_apoplast_X.mp4",writer=writer)
