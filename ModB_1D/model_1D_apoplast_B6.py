import sys
import numpy as np
from assimulo.solvers import CVode
from assimulo.problem import Explicit_Problem

class Mod1D():
    def __init__(self,N,K,PBC):

        self.T=[1]
        self.pbc=PBC

        #global
        self.n=N
        self.K=K
        self.verb=50
    
        #auxin
        self.Ga=1
        self.A=6
        self.Da=35.5
        self.Da_sym=1
        self.Da_apo=1
        
        #pin
        self.Gp=2
        self.p=40
        self.Ep=1
        
        #phi
        self.m=1.1
        self.mm=1
        
        # extracellular space
        self.Eq=1
        self.q=1
        self.V=10
        
        #molecule X
        self.Gx=1
        self.Dx=1
        self.r=1
    
        
        
    def progress(self,t,T=0):
        T=self.T[0]
        prog="\r{0:.4f}%".format(t/T*100)
        sys.stdout.write(prog+" "*(25-len(prog)))
    
    # shift array    
    def shift(self,arr,d):
        if d: out=np.array(list(arr[1:])+[arr[0]*self.pbc]) # right
        else: out=np.array([arr[-1]*self.pbc]+list(arr[:-1])) # left
        return out
    
    # PIN polarization function    
    def phia(self,aux):
        return aux**self.m
    
    def phix(self,apox):
        return apox**self.mm
    
    def theta(self,aux):
        return (2*aux**self.r)/(self.A**self.r + aux**self.r)
    
    # flux functions 
    def flux_aux(self,aux,apo,pinl,pinr):
        return self.Ep*aux*(pinl+pinr)-self.Eq*self.q*(self.shift(apo,0)+apo)
    
    def flux_apo(self,aux,apo,pinl,pinr):
        return 2*self.Eq*self.q*apo-self.Ep*(pinr*aux+self.shift(pinl,1)*self.shift(aux,1))
    
    # diffusion functions  
    def diff(self,aux,apo):
        return self.Da*(self.shift(apo,0)+apo-2*aux)
    
    def apodiff(self,aux,apo):
        return self.Da*(self.shift(aux,1)+aux-2*apo)
        
    def cytxdiff(self,cytx,apox):
        return self.Dx*(self.shift(apox,0)+apox-2*cytx)
    
    def apoxdiff(self,cytx,apox):
        return self.Dx*(self.shift(cytx,1)+cytx-2*apox)
    
        # symplast duffusion
    def sym_diff(self,aux):
        return self.Da_sym*(self.shift(aux,0)+self.shift(aux,1)-2*aux)
    
        # apoplast diffusion
    def apo_diff(self,apo):
        return (self.Da_apo/self.V)*(self.shift(apo,0)+self.shift(apo,1)-2*apo)

    # ODE 
    def sim(self,time,start):
        
        # progress of integration
        self.progress(time)

        aux,apo,pinl,pinr,cytx,apox=np.array_split(start,6)

        dta=self.Ga*(self.A-aux)-self.flux_aux(aux,apo,pinl,pinr)+self.diff(aux,apo)+self.sym_diff(aux)
        dtapo=-self.flux_apo(aux,apo,pinl,pinr)/self.V+self.apodiff(aux,apo)/self.V+self.apo_diff(apo)
        dtpl=self.Gp*(self.p*(self.phix(self.shift(apox,0))/(self.phix(self.shift(apox,0))+self.phix(apox)))-pinl)
        dtpr=self.Gp*(self.p*(self.phix(apox)/(self.phix(self.shift(apox,0))+self.phix(apox)))-pinr)
        
        dtcx=self.Gx*(self.theta(aux)-cytx)+self.cytxdiff(cytx,apox)
        dtax=-self.Gx*apox+self.apoxdiff(cytx,apox)/self.V

        out=np.concatenate((dta,dtapo,dtpl,dtpr,dtcx,dtax))
        return out
    
    def intgr(self,time,start):
        self.T[0]=time
        
        # CVode from Assimulo                
        problem=Explicit_Problem(self.sim,start)
        sim=CVode(problem)
        sim.verbosity=self.verb
        T,res=sim.simulate(time,time)

        return [T,res]    

