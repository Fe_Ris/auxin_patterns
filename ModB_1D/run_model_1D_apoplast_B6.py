from model_1D_apoplast_B6 import Mod1D
import sys
import numpy as np
import pylab as plt
import random

n=100
k=2

auxin=2
pin=2
X=1
aX=.1

time=10000
random.seed(123)

# base auxin and pin with random noise
noise=.01

aux = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
apo = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
pin_left = [pin for i in range(n)] + np.random.normal(0,pin*noise,n)
pin_right = [2*pin-x for x in pin_left] + np.random.normal(0,pin*noise,n)

cytx=[X for i in range(n)] + np.random.normal(0,X*noise,n)
apox=[aX for i in range(n)] + np.random.normal(0,aX*noise,n)

#set noise manually
#aux[50]*=1.1

start=np.concatenate((aux, apo, pin_left, pin_right, cytx, apox))

PBC=0

# class initiation
model=Mod1D(n,k,PBC)

model.Ga=.2 # auxin degredation/synthesis coefficient to A
model.A=1 # auxin base level of synthesis (steady state)
model.Da=1 # auxin diffusion coefficient to equilibrium
model.Da_sym=0 # symplast diffusion coefficient (cell-cell)
model.Da_apo=0 # apoplast diffusion coefficient (apoplast-apoplast)

model.Gp=1 # pin degredation/synthesis coefficient to p
model.p=10 # pin base level of synthesis (steady state)
model.m=4 # value of regulatory function phi
model.Ep=1 # pin efflux efficiency

model.q=10 # concentration of influx carriert on membrane side
model.Eq=1 # efficiency of influx carrier
model.V=1 # volume ratio of apoplast to cytoplast

model.Gx=1 # molecule X degredation/synthesis coefficient 
model.Dx=10 # molecule X diffusion coefficient
model.r=1 # value of regulatory function theta
model.mm=6 # phi with molecule x



# integration
T,R=model.intgr(time,start)
#print(model.sim(time,start))
peaks=[]
def show_prange(param,ran):
    p0,p1,dp=ran
    plt.figure(figsize=((10,8)))
    for p in np.arange(p0,p1,dp):
        model.p=p
        T,R=model.intgr(time,start)
        peaks.append(max(R[-1,:n]))

        
    newp0=[x for i,x in enumerate(np.arange(p0,p1,dp)) if peaks[i]<=model.A+.01][-1]
    newp1=[x for i,x in enumerate(np.arange(p0,p1,dp)) if peaks[i-1]<peaks[i]*.95][0]
    newdp=abs((newp0-newp1)/5)
    #return [newp0,newp1,newdp]
    if newdp<.01:
        plt.plot(range(n),R[-1,:n],label=param+"= {0:3.4f}".format(p))
        plt.legend()  
        print(newp0,newp1) 
        sys.exit()         
    show_prange(param,[newp0,newp1,newdp])   



# plots
#    concentration of auxin and PIN over time
def CT(R=R):
    plt.plot(range(time),R[1:,:n])
    plt.title("Auxin")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.xlim((0,50))
    #plt.ylim((1.6,2.2))
    plt.show()
    
    plt.plot(range(time),R[1:,n:2*n])
    plt.title("PIN_left")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()
    
    plt.plot(range(time),R[1:,2*n:])
    plt.title("PIN_right")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()
    
    

#   concentration of auxin in cytosol and apoplast over all cells (lines: show curve plot; bars: show barplot)
def apo(lines=0,bars=1,R=R):
    fig=plt.figure(figsize=(15,5))
    ax=plt.subplot(111)
    ind=np.arange(1,n+1)
    bw=0.35
    plt.plot(range(1,n+1),R[0,:n],color="r",label="initial auxin levels")
    if bars:
        ax.bar(ind+bw,R[-1,n:2*n],bw,label="apoplast aux",color="darkorange")
        ax.bar(ind,R[-1,:n],bw,label="cytoplast aux",color="cornflowerblue")
    if lines:
        ax.plot(ind+bw,R[-1,n:2*n],"darkorange")
        ax.plot(ind,R[-1,:n],"cornflowerblue")
    ax.set_xticks([int(ind[i-1]+bw/2) for i in range(1,n+1) if i%5==0])
    plt.legend()
    #plt.title(title)
    fig.tight_layout()
    plt.show()
    
#   barplot of auxin levels over all cells (base: show initial auxin distribution)
def pattern(R=R,base=1):
    print()
    print("max:","{0:.3f}".format(max(R[-1,:n])))
    print("min:","{0:.3f}".format(min(R[-1,:n])))
    
    fig=plt.figure(figsize=(15,5))
    plt.bar(range(n),R[-1,:n],color="lightblue",label="auxin levels at steady state")
    if base: plt.plot(range(n),start[:n],"r",label="initial auxin levels")
    plt.ylabel("auxin concentration")
    plt.xlabel("cell position")
    if n>50: plt.xticks(range(0,n,5))
    else: plt.xticks(range(1,n+1))
    plt.title("Auxin pattern (noise: {}%)".format(noise*100))
    plt.legend(loc="lower left")
    plt.show()

# select plots to be shown after integration
#CT()
#pattern()
apo(0,1)
