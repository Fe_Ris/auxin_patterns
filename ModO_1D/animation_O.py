from run_model_simple1D import R,n
#from run_model_simple1D_inh import R,n
import numpy as np
import pylab as plt
from matplotlib import animation


fig=plt.figure(figsize=(15,5))
ax=plt.axes()
ax2=ax.twinx()

ax.set_xlabel("cells")
ax.set_ylabel("Auxin concentration")
ax2.set_ylabel("PIN concentration")

ax.set_ylim((0,20))
ax2.set_ylim((0,100))

bar = ax.bar(range(n),R[0,:n],color="cornflowerblue",label="auxin (cytoplast)")
line_pl, = ax2.plot(range(n),R[0,n:2*n],color="r",label="PIN left")
line_pr, = ax2.plot(range(n),R[0,2*n:],color="g",label="PIN right")

all=[bar,line_pl,line_pr]
labels=[x.get_label() for x in all]
plt.legend(all,labels)

def animate(i):
    [bar[j].set_height(R[i,j]) for j in range(n)]
    line_pl.set_data(range(n),R[i,n:2*n])
    line_pr.set_data(range(n),R[i,2*n:])
    ax.set_title("t="+str(i))
    return bar

Writer=animation.writers['ffmpeg']
writer=Writer(fps=5)

ani=animation.FuncAnimation(fig,animate,frames=250)
print("Don't forget to enter a name!")
#ani.save("1D_simple_PBC"+".mp4",writer=writer)

