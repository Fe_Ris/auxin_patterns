import sys
import numpy as np
from assimulo.solvers import CVode
from assimulo.problem import Explicit_Problem

class Mod1D():
    def __init__(self,N,K,PBC,acc=0.1):

        self.T=[1]
        self.PBC=PBC
        self.acc=acc
        self.stst_time=[0]
        self.found=[0]
        self.hold=0
        
        #global
        self.n=N
        self.K=K
        self.verb=50
    
        #auxin
        self.Ga=1
        self.A=6
        self.Da=35.5
        
        #pin
        self.Gp=2
        self.p=40
        self.Ep=1
        
        #phi
        self.m=1.1
    
    # integration progress 
    def progress(self,t,T=0):
        T=self.T[0]
        prog="\r{0:.4f}%".format(t/T*100)
        sys.stdout.write(prog+" "*(25-len(prog)))

    # shift array
    def shift(self,arr,d):
        if d: out=np.array(list(arr[1:])+[arr[0]*self.PBC]) # right
        else: out=np.array([arr[-1]*self.PBC]+list(arr[:-1])) # left
        return out        

    # PIN polarization function   
    def phi(self,aux):
        return aux**self.m
    
    # flux function
    def flux(self,aux,pinl,pinr):  
        return self.Ep*((pinr+pinl)*aux-self.shift(pinl,1)*self.shift(aux,1)-self.shift(pinr,0)*self.shift(aux,0))
    
    # diffusion function
    def diff(self,aux):
        return self.Da*(self.shift(aux,1)+self.shift(aux,0)-2*aux)

    # ODEs
    def sim(self,time,start):
        
        # progress of integration
        self.progress(time)
        
        aux=start[:self.n]
        pinl=start[self.n:2*self.n]
        pinr=start[2*self.n:3*self.n]
        pstop=start[3*self.n:]


        
        dta=(self.Ga*(self.A-aux)-self.flux(aux,pinl,pinr)+self.diff(aux))*pstop
        dtpl=self.Gp*(self.K*self.p*(self.phi(self.shift(aux,0))/(self.phi(self.shift(aux,0))+self.phi(self.shift(aux,1))))-pinl)
        dtpr=self.Gp*(self.K*self.p*(self.phi(self.shift(aux,1))/(self.phi(self.shift(aux,0))+self.phi(self.shift(aux,1))))-pinr)
        
                
        out=np.concatenate((dta,dtpl,dtpr,np.zeros(self.n)))
        return out
        
    def intgr(self,time,start):
        self.T[0]=time
        
        # CVode from Assimulo                
        problem=Explicit_Problem(self.sim,start)
        sim=CVode(problem)
        sim.verbosity=self.verb
        T,res=sim.simulate(time,time)

        return [T,res]    
