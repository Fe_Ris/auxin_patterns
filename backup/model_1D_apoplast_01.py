import sys
import numpy as np
from assimulo.solvers import CVode
from assimulo.problem import Explicit_Problem

class Mod1D():
    def __init__(self,N,K):

        self.T=[1]

        
        
        #global
        self.n=N
        self.K=K
        self.verb=50
    
        #auxin
        self.Ga=1
        self.A=6
        self.Da=35.5
        self.Da_sym=1
        self.Da_apo=1
        
        #pin
        self.Gp=2
        self.p=40
        self.Ep=1
        
        #phi
        self.m=1.1
        
        # extracellular space
        self.Eq=1
        self.q=1
        self.V=10
    
        
    def progress(self,t,T=0):
        T=self.T[0]
        prog="\r{0:.4f}%".format(t/T*100)
        sys.stdout.write(prog+" "*(25-len(prog)))
    
    # shift array    
    def shift(self,arr,d):
        if d: out=np.array(list(arr[1:])+[arr[0]]) # right
        else: out=np.array([arr[-1]]+list(arr[:-1])) # left
        return out
        
    def phi(self,aux):
        return aux**self.m
    
    # flux functions (0 without apoplast)
    def flux0(self,aux,pinl,pinr):  
        return self.Ep*((pinr+pinl)*aux-self.shift(pinl,1)*self.shift(aux,1)-self.shift(pinr,0)*self.shift(aux,0))
    
    def flux1_aux(self,aux,apo,pinl,pinr):
        return self.Ep*aux*(pinl+pinr)-self.Eq*self.q*(self.shift(apo,0)+self.shift(apo,1))
    
    def flux1_apo(self,aux,apo,pinl,pinr):
        return 2*self.Eq*self.q*apo-self.Ep*(pinr*aux+self.shift(pinl,1)*self.shift(aux,1))
    
    # diffusion functions (0 without apoplast)
    def diff0(self,aux):
        return self.Da*(self.shift(aux,1)+self.shift(aux,0)-2*aux)
    
    def diff1(self,aux,apo):
        return 
    
    # symplast duffusion
    def sym_diff(self,aux):
        return self.Da_sym*(self.shift(aux,0)+self.shift(aux,1)-2*aux)
    
    # apoplast diffusion
    def apo_diff(self,apo):
        return (self.Da_apo/self.V)*(self.shift(apo,0)+self.shift(apo,1)-2*apo)
    
    # ODE without apoplast    
    def sim0(self,time,start):
        
        # progress of integration
        self.progress(time)

        aux=start[:self.n]
        pinl=start[self.n:2*self.n]
        pinr=start[2*self.n:]

        
        dta=self.Ga*(self.A-aux)-self.flux_arr(aux,pinl,pinr)+self.diff_arr(aux)
        dtpl=self.Gp*(self.K*self.p*(self.phi(self.shift(aux,0))/(self.phi(self.shift(aux,0))+self.phi(self.shift(aux,1))))-pinl)
        dtpr=self.Gp*(self.K*self.p*(self.phi(self.shift(aux,1))/(self.phi(self.shift(aux,0))+self.phi(self.shift(aux,1))))-pinr)

        out=np.concatenate((dta,dtpl,dtpr))
        return out
    
    # ODE with apoplast
    def sim1(self,time,start):
        
        # progress of integration
        self.progress(time)

        aux,apo,pinl,pinr=np.array_split(start,4)

        dta=self.Ga*(self.A-aux)-self.flux1_aux(aux,apo,pinl,pinr)+self.Da*(self.shift(apo,0)+apo-2*aux)+self.sym_diff(aux)
        dtapo=-self.flux1_apo(aux,apo,pinl,pinr)/self.V+self.Da/self.V*(self.shift(aux,0)+aux-2*apo)+self.apo_diff(apo)
        dtpl=self.Gp*(self.K*self.p*(self.phi(self.shift(apo,0))/(self.phi(self.shift(apo,0))+self.phi(apo)))-pinl)
        dtpr=self.Gp*(self.K*self.p*(self.phi(apo)/(self.phi(self.shift(apo,0))+self.phi(apo)))-pinr)

        out=np.concatenate((dta,dtapo,dtpl,dtpr))
        return out
    
    def intgr(self,time,start):
        self.T[0]=time

        
        # CVode from Assimulo                
        problem=Explicit_Problem(self.sim1,start)
        sim=CVode(problem)
        sim.verbosity=self.verb
        T,res=sim.simulate(time,time)

        return [T,res]    

