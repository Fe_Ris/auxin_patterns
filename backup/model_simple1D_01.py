import sys
import numpy as np
from assimulo.solvers import CVode
from assimulo.problem import Explicit_Problem

class Mod1D():
    def __init__(self,N,K,M):

        self.T=[1]
        self.M=[self.sim,self.sim_arr][M]
        
        #global
        self.n=N
        self.K=K
        self.verb=50
    
        #auxin
        self.Ga=1
        self.A=6
        self.Da=35.5
        
        #pin
        self.Gp=2
        self.p=40
        self.Ep=1
        
        #phi
        self.m=1.1
    
    def progress(self,t,T=0):
        T=self.T[0]
        prog="\r{0:.4f}%".format(t/T*100)
        sys.stdout.write(prog+" "*(25-len(prog)))
        
    def phi(self,aux):
        return aux**self.m
    
    def shift(self,arr,d):
        if d: out=np.array(list(arr[1:])+[arr[0]]) # right
        else: out=np.array([arr[-1]]+list(arr[:-1])) # left
        return out
    
    def flux(self,aux,pinl,pinr,i):
        return self.Ep*((pinr[i]+pinl[i])*aux[i]-pinl[(i+1)%self.n]*aux[(i+1)%self.n]-pinr[i-1]*aux[i-1])
    
    def flux_arr(self,aux,pinl,pinr):  
        return self.Ep*((pinr+pinl)*aux-self.shift(pinl,1)*self.shift(aux,1)-self.shift(pinr,0)*self.shift(aux,0))
    
    def diff_arr(self,aux):
        return self.Da*(self.shift(aux,1)+self.shift(aux,0)-2*aux)
        
    def sim(self,time,start):
        
        # progress of integration
        self.progress(time)
        
        aux=start[:self.n]
        pinl=start[self.n:2*self.n]
        pinr=start[2*self.n:]

        dta,dtpl,dtpr=[],[],[]
        
        for i in range(self.n):
            dta.append(self.Ga*(self.A-aux[i])-self.flux(aux,pinl,pinr,i)+self.Da*(aux[i-1]+aux[(i+1)%self.n]-2*aux[i]))
            dtpl.append(self.Gp*(self.K*self.p*(self.phi(aux[i-1])/(self.phi(aux[i-1])+self.phi(aux[(i+1)%self.n])))-pinl[i]))
            dtpr.append(self.Gp*(self.K*self.p*(self.phi(aux[(i+1)%self.n])/(self.phi(aux[i-1])+self.phi(aux[(i+1)%self.n])))-pinr[i]))

            
        out=np.concatenate((dta,dtpl,dtpr))
        return out
    
    def sim_arr(self,time,start):
        
        # progress of integration
        self.progress(time)
        
        #start=np.array(start)
        aux=start[:self.n]
        pinl=start[self.n:2*self.n]
        pinr=start[2*self.n:]

        #dta,dtpl,dtpr=[np.zeros(self.n) for i in range(3)]
        
        dta=self.Ga*(self.A-aux)-self.flux_arr(aux,pinl,pinr)+self.diff_arr(aux)
        dtpl=self.Gp*(self.K*self.p*(self.phi(self.shift(aux,0))/(self.phi(self.shift(aux,0))+self.phi(self.shift(aux,1))))-pinl)
        dtpr=self.Gp*(self.K*self.p*(self.phi(self.shift(aux,1))/(self.phi(self.shift(aux,0))+self.phi(self.shift(aux,1))))-pinr)
        
#        dta[0]=self.Ga*(self.A-aux[0])-self.Ep*(pinr[0]*aux[0]-pinl[1]*aux[1])+self.Da*(aux[1]-aux[0])
#        dta[-1]=self.Ga*(self.A-aux[-1])-self.Ep*(pinl[-1]*aux[-1]-pinr[-2]*aux[-2])+self.Da*(aux[-2]-aux[-1])
#        
#        dtpl[0]=-self.Gp*pinl[0]
#        dtpr[0]=self.Gp*(self.K*self.p-pinr[0])
#        
#        dtpr[-1]=-self.Gp*pinr[-1]
#        dtpl[-1]=self.Gp*(self.K*self.p-pinl[-1])
        
#        print(dtpl)
        out=np.concatenate((dta,dtpl,dtpr))
        return out
        
    def intgr(self,time,start):
        self.T[0]=time
        
        # CVode from Assimulo                
        problem=Explicit_Problem(self.M,start)
        sim=CVode(problem)
        sim.verbosity=self.verb
        T,res=sim.simulate(time,time)

        return [T,res]    
