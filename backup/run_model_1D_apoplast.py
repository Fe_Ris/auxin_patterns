from model_1D_apoplast_01 import Mod1D
import numpy as np
import pylab as plt

n=40
k=2

auxin=1
pin=1

time=10000

# base auxin and pin with random noise
noise=.01

aux = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
apo = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
pin_left = [pin for i in range(n)] + np.random.normal(0,pin*noise,n)
pin_right = [2*pin-x for x in pin_left] + np.random.normal(0,pin*noise,n)

#set noise manually
#aux[50]*=1.1

start=np.concatenate((aux, apo, pin_left, pin_right))

# class initiation
model=Mod1D(n,k)

model.Ga=.2 # auxin degredation/synthesis coefficient to A
model.A=2 # auxin base level of synthesis (steady state)
model.Da=30 # auxin diffusion coefficient to equilibrium
model.Da_sym=0 # symplast diffusion coefficient (cell-cell)
model.Da_apo=0 # apoplast diffusion coefficient (apoplast-apoplast)

model.Gp=1 # pin degredation/synthesis coefficient to p
model.p=30 # pin base level of synthesis (steady state)
model.m=4 # influence of regulatory function phi
model.Ep=1 # pin efflux efficiency

model.q=30 # concentration of influx carriert on membrane side
model.Eq=1 # efficiency of influx carrier
model.V=1 # volume ratio of apoplast to cytoplast

# integration
T,R=model.intgr(time,start)
#print(model.sim(time,start))

# plots
#    concentration of auxin and PIN over time
def CT(R=R):
    [plt.plot(range(time),R[1:,:n])]
    plt.title("Auxin")
    plt.xlabel("concentration")
    plt.ylabel("time")
    #plt.xlim((0,100))
    #plt.ylim((1.6,2.2))
    plt.show()
    
    plt.plot(range(time),R[1:,n:2*n])
    plt.title("PIN_left")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()
    
    plt.plot(range(time),R[1:,2*n:])
    plt.title("PIN_right")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()

#   concentration of auxin in cytosol and apoplast over all cells (lines: show curve plot; bars: show barplot)
def apo(lines=0,bars=1,R=R):
    fig=plt.figure(figsize=(10,5))
    ax=plt.subplot(111)
    ind=np.arange(1,n+1)
    bw=0.35
    if bars:
        ax.bar(ind,R[-1,n:2*n],bw,label="apoplast aux")
        ax.bar(ind+bw,R[-1,:n],bw,label="cytoplast aux")
    if lines:
        ax.plot(range(n),R[-1,n:2*n],"b",label="apo")
        ax.plot(range(n),R[-1,:n],"orange",label="cyto")
    ax.set_xticks([int(ind[i-1]+bw/2) for i in range(1,n+1) if i%5==0])
    plt.legend()
    fig.tight_layout()
    plt.show()
    
#   barplot of auxin levels over all cells (base: show initial auxin distribution)
def pattern(R=R,base=1):
    print()
    print("max:","{0:.3f}".format(max(R[-1,:n])))
    print("min:","{0:.3f}".format(min(R[-1,:n])))
    
    fig=plt.figure(figsize=(15,5))
    plt.bar(range(n),R[-1,:n],color="lightblue",label="auxin levels at steady state")
    if base: plt.plot(range(n),start[:n],"r",label="initial auxin levels")
    plt.ylabel("auxin concentration")
    plt.xlabel("cell position")
    if n>50: plt.xticks(range(0,n,5))
    else: plt.xticks(range(1,n+1))
    plt.title("Auxin pattern (noise: {}%)".format(noise*100))
    plt.legend(loc="lower left")
    plt.show()

# select plots to be shown after integration
CT()
pattern()
apo(0,1)
