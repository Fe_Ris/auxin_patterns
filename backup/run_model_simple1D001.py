from model_simple1D import Mod1D
import itertools
import numpy as np
import pylab as plt

# total cells
n=100
# neighbours per cell (2 for 1D)
k=2

# auxin and PIN initial base concentration
auxin=6
pin=4

# integration time
time=10000

# base auxin and pin with random noise
noise=.01

aux = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
pin_left = [pin for i in range(n)] + np.random.normal(0,pin*noise,n)
pin_right = [2*pin-x for x in pin_left] + np.random.normal(0,pin*noise,n)

#set noise manually
#aux[20]*=1.1

start=np.concatenate((aux, pin_left, pin_right))

# switch periodic boundary condition
noPBC=0

# class initiation 
model=Mod1D(n,k,noPBC)

model.Ga=1 # auxin degredation/synthesis rate to A
model.A=6 # auxin base level of synthesis (steady state)
model.Da=35.5 # auxin diffusion rate to equilibrium

model.Gp=2 # pin degredation/synthesis rate to p
model.p=40 # pin base level of synthesis (steady state)
model.m=1.1 # influence of regulatory function phi
model.Ep=1 # pin efflux efficiency

# integration
T,R=model.intgr(time,start)

# wavelength of auxin pattern
def wave(R=R):
    DFT=[abs(x)**2 for x in np.fft.fft(R[-1,:n])[1:int(n/2)]]
    k=list(DFT).index(max(DFT))+1
    return n/k 

# average auxin peak width
def Pwid(R=R):
    aux=R[-1,:n]
    avg=np.average(aux)
    high=[1 if aux[i]>avg else 0 for i in range(n)]
    return np.average([sum(1 for _ in group) for key,group in itertools.groupby(high) if key])

# plots
#    concentration of auxin and PIN over time
def CT(R=R):
    [plt.plot(range(time),R[1:,:n])]
    plt.title("Auxin")
    plt.xlabel="concentration"
    plt.ylabel="time"
    plt.show()
    
    plt.plot(range(time),R[1:,n:2*n])
    plt.title("PIN_left")
    plt.xlabel="concentration"
    plt.ylabel="time"
    plt.show()
    
    plt.plot(range(time),R[1:,2*n:])
    plt.title("PIN_right")
    plt.xlabel="concentration"
    plt.ylabel="time"
    plt.show()

#   barplot of auxin levels over all cells (base: show initial auxin distribution)    
def pattern(R=R,base=1,avg=0):
    print()
    print("max:","{0:.3f}".format(max(R[-1,:n])))
    print("min:","{0:.3f}".format(min(R[-1,:n])))
    print("\n","wavelength="+str(wave()))
    print(" average peak width="+str(Pwid()))
    
    
    fig=plt.figure(figsize=(15,5))
    plt.bar(range(n),R[-1,:n],color="lightblue",label="auxin levels at steady state")
    if base: plt.plot(range(n),start[:n],"r",label="initial auxin levels")
    if avg: plt.plot(range(n),[np.average(R[-1,:n])]*n,label="average auxin level")
    plt.ylabel("auxin concentration")
    plt.xlabel("cell position")
    if n>50: plt.xticks(range(0,n,5))
    else: plt.xticks(range(1,n+1))
    plt.title("Auxin pattern (noise: {}%)".format(noise*100))
    plt.legend(loc="upper left")
    plt.show()

# select plots to be shown after integration
CT()
pattern()