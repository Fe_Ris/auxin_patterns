from model_simple1D import Mod1D
import sys
import itertools
import numpy as np
import pylab as plt
 

n=100
k=2

auxin=2
pin=4

time=10000
method=1

noise=.01

# auxin
aux = [auxin for i in range(n)] + np.random.normal(0,auxin*noise,n)
# PINs (both sides)
pin_left = [pin for i in range(n)] + np.random.normal(0,pin*noise,n)
pin_right = [2*pin-x for x in pin_left] + np.random.normal(0,pin*noise,n)

#aux[40]*=1.2

start=np.concatenate((aux, pin_left, pin_right))

model=Mod1D(n,k,method)

model.Ga=1 # auxin degredation/synthesis rate to A
model.A=6 # auxin base level of synthesis (steady state)
model.Da=35.5 # auxin diffusion rate to equilibrium

model.Gp=2 # pin degredation/synthesis rate to p
model.p=40 # pin base level of synthesis (steady state)
model.m=1.1 # influence of regulatory function phi
model.Ep=1 # pin efflux efficiency

# integration
T,R=model.intgr(time,start)

# wavelength of auxin peaks in the pattern
def wave(R=R):
    DFT=[abs(x)**2 for x in np.fft.fft(R[-1,:n])[1:int(n/2)]]
    k=list(DFT).index(max(DFT))+1
    return n/k 

# average width of auxin peaks in the pattern
def Pwid(R=R):
    aux=R[-1,:n]
    avg=np.average(aux)
    high=[1 if aux[i]>avg else 0 for i in range(n)]
    return np.average([sum(1 for _ in group) for key,group in itertools.groupby(high) if key])

# distribution of auxin and PIN over cells            
def Dist(R=R):
    ax=plt.figure(figsize=(15,5))
    ax=plt.subplot(111)
    ax.plot(range(n),R[-1,:n],label="auxin")
    ax.plot(range(n),R[-1,n:2*n],label="left PIN")
    ax.plot(range(n),R[-1,2*n:],label="right PIN")
    plt.title("auxin and PIN distribution at steady state")
    plt.ylabel("auxin/PIN concentration")
    plt.xlabel("cell locations")
    plt.legend()
    plt.show()

# concentration of auxin and PINs (both side) over time
def CT(R=R):
    plt.plot(range(time),R[1:,:n])
    plt.title("Auxin")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()
    
    plt.plot(range(time),R[1:,n:2*n])
    plt.title("PIN_left")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()
    
    plt.plot(range(time),R[1:,2*n:])
    plt.title("PIN_right")
    plt.xlabel("concentration")
    plt.ylabel("time")
    plt.show()

# barplot of final auxin pattern (base: show initial auxin distribution)    
def pattern(R=R,base=1,avg=0):
    
    print("\n")
    print("max: {0:.3f}".format(max(R[-1,:n])))
    print("min: {0:.3f}".format(min(R[-1,:n])))
    print("avg: {0:.3f}".format(np.average(R[-1,:n])),"\n")
    print("wavelength:         {0:.3f}".format(wave()))
    print("average peak width: {0:.3f}".format(Pwid()))
        
    fig=plt.figure(figsize=(15,5))
    plt.bar(range(n),R[-1,:n],color="lightblue",label="auxin levels at steady state")
    if base: plt.plot(range(n),start[:n],"r",label="initial auxin levels")
    if avg: plt.plot(range(n),[np.average(R[-1,:n])]*n,label="average auxin concentration")
    plt.ylabel("auxin concentration")
    plt.xlabel("cell position")
    if n>50: plt.xticks(range(0,n,5))
    else: plt.xticks(range(1,n+1))
    plt.title("Auxin pattern (noise: {}%)".format(noise*100))
    plt.legend(loc="upper left")
    plt.show()

# progress of integration (timestep/total time)
def progress(t,T):
    prog="\r{0:.4f}%".format(t/T*100)
    sys.stdout.write(prog+" "*(25-len(prog)))

# show plots after integration
#PIN()
#CT()
pattern(base=1)
    

